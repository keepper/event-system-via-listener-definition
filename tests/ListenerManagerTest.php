<?php

namespace Keepper\Lib\EventSystem\Tests;


use Keepper\Lib\EventSystem\Contracts\ListenerInterface;
use Keepper\Lib\EventSystem\Contracts\ListenerProviderInterface;
use Keepper\Lib\EventSystem\ListenerManager;
use Keepper\Lib\EventSystem\StrictListenerManager;
use Keepper\Lib\EventSystem\Tests\Fixtures\TestListener;
use Keepper\Lib\EventSystem\Tests\Fixtures\TestTwoHandlerInterface;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ListenerManagerTest extends TestCase {

	protected $manager;

	/**
	 * @var ListenerProviderInterface
	 */
	protected $strict;

	protected function getManager(): ListenerManager {
		if ( !is_null($this->manager) ) {
			return $this->manager;
		}
		$logger = new Logger('UnitTest');
		$this->strict = $this->strict = new StrictListenerManager();
		$this->manager = new ListenerManager($this->strict);
		$this->manager->setLogger($logger);
		return $this->manager;
	}

	public function testReplaceListenerMeta() {
		$this->getManager()->addListenerMeta(TestTwoHandlerInterface::class, 'someOneMethod', 'someEvent');
		$this->getManager()->addListenerMeta(TestTwoHandlerInterface::class, 'onTwoEvent', 'someEvent');

		$this->getManager()->addListener(new TestListener());

		$this->assertTrue($this->strict->hasListeners('someEvent'));
		$this->assertEquals('onTwoEvent', $this->strict->getListeners('someEvent')[0][1]);
	}

	public function testLogErrorOnAddingMeta() {
		$logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
		$logger->expects($this->once())->method('error');
		$this->getManager()->setLogger($logger);

		$this->getManager()->addListenerMeta(ListenerInterface::class, '', '');
	}

	public function testLogErrorOnAddListener() {
		$logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
		$logger->expects($this->once())->method('warning');
		$this->getManager()->setLogger($logger);

		$this->getManager()->addListener(new class implements ListenerInterface {});
	}

	public function testLogErrorOnRemoveListener() {
		$logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
		$logger->expects($this->once())->method('warning');
		$this->getManager()->setLogger($logger);

		$this->getManager()->removeListener(new class implements ListenerInterface {});
	}
}