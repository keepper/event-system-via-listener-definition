<?php
namespace Keepper\Lib\EventSystem\Tests\Dispatcher;

use Keepper\Lib\EventSystem\Contracts\ListenerProviderInterface;
use Keepper\Lib\EventSystem\Dispatcher\OneWayNotificationDispatcher;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class OneWayNotificationDispatcherTest extends TestCase {

	public function testLogMessageWithoutListeners () {
		$eventName = 'someEvent' . rand(1,100);

		$provider = $this->getMockBuilder(ListenerProviderInterface::class)->getMock();
		$provider->method('hasListeners')->with($eventName)->willReturn(false);
		$provider->method('getListeners')->with($eventName)->willReturn([]);

		$logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
		$logger->expects($this->once())->method('debug');

		$dispatcher = new OneWayNotificationDispatcher($provider);
		$dispatcher->setLogger($logger);

		$dispatcher->dispatch($eventName, 1, 2);
	}

	public function testLogMessageOnListenerException () {
		$eventName = 'someEvent' . rand(1,100);
		$isSecondListenerCalled = false;

		$provider = $this->getMockBuilder(ListenerProviderInterface::class)->getMock();
		$provider->method('hasListeners')->with($eventName)->willReturn(true);
		$provider->method('getListeners')->with($eventName)->willReturn([
			function ($a, $b) {
				$this->assertEquals(1, $a);
				$this->assertEquals('two', $b);
				throw new \Exception('Test Exception');
			},
			function (...$args) use (&$isSecondListenerCalled) {
				$isSecondListenerCalled = true;
			}
		]);

		$logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
		$logger->expects($this->once())->method('debug');
		$logger->expects($this->once())->method('error');

		$dispatcher = new OneWayNotificationDispatcher($provider);
		$dispatcher->setLogger($logger);

		$dispatcher->dispatch($eventName, 1, 'two');

		$this->assertTrue($isSecondListenerCalled, 'Ожидали, что несмотря на исключение, второй слушатель будет вызван');
	}
}