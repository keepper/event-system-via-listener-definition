<?php

namespace Keepper\Lib\EventSystem\Tests\Fixtures;

use Keepper\Lib\EventSystem\Contracts\ListenerInterface;

interface TestTwoHandlerInterface extends ListenerInterface {

	public function onTwoEvent();
}