<?php
namespace Keepper\Lib\EventSystem\Tests\Fixtures;

use Keepper\Lib\EventSystem\Contracts\ListenerInterface;

interface TestOneHandlerInterface extends ListenerInterface {

	public function onOneEvent();
}