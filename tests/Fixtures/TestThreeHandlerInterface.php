<?php

namespace Keepper\Lib\EventSystem\Tests\Fixtures;

use Keepper\Lib\EventSystem\Contracts\ListenerInterface;

interface TestThreeHandlerInterface extends ListenerInterface {

	public function onThreeEvent();

}