<?php
namespace Keepper\Lib\EventSystem\Tests;

use Keepper\Lib\EventSystem\Contracts\ListenerInterface;
use Keepper\Lib\EventSystem\StrictListenerManager;
use Keepper\Lib\EventSystem\Tests\Fixtures\TestListener;
use Keepper\Lib\EventSystem\Tests\Fixtures\TestOneHandlerInterface;
use Keepper\Lib\EventSystem\Tests\Fixtures\TestThreeHandlerInterface;
use Keepper\Lib\EventSystem\Tests\Fixtures\TestTwoHandlerInterface;
use PHPUnit\Framework\TestCase;

class StrictListenerManagerTest extends TestCase {

	protected $manager;

	protected function getManager(): StrictListenerManager {
		return $this->manager ?? $this->manager = new StrictListenerManager();
	}

	/**
	 * @expectedException \Keepper\Lib\EventSystem\Exceptions\DuplicateListenerMetaException
	 */
	public function testAddDoubleMeta () {
		$this->getManager()->addListenerMeta(\stdClass::class, 'someMethod', 'someEvent');
		$this->getManager()->addListenerMeta(\stdClass::class, 'someMethod', 'someEvent');
	}

	/**
	 * @expectedException \Keepper\Lib\EventSystem\Exceptions\EventSystemException
	 */
	public function testMarkerInterfaceMeta() {
		$this->getManager()->addListenerMeta(ListenerInterface::class, 'someMethod', 'someEvent');
	}

	/**
	 * @expectedException \Keepper\Lib\EventSystem\Exceptions\HandlerMetaDataException
	 */
	public function testUnknownMetaException() {
		$listener = $this->getMockBuilder(ListenerInterface::class)->getMock();
		$this->getManager()->addListener($listener);
	}

	/**
	 * @expectedException \Keepper\Lib\EventSystem\Exceptions\HandlerMetaDataException
	 */
	public function testIncorrectMeta() {
		$this->getManager()->addListenerMeta(TestOneHandlerInterface::class, 'someUnexistMethod', 'oneEvent');
		$listener = new TestListener();
		$this->getManager()->addListener($listener);
	}

	/**
	 * @expectedException \Keepper\Lib\EventSystem\Exceptions\HandlerNotListenException
	 */
	public function testUnListenListener() {
		$this->getManager()->addListenerMeta(TestOneHandlerInterface::class, 'onOneEvent', 'oneEvent');

		$listener = new TestListener();
		$this->getManager()->removeListener($listener);
	}

	public function testAddListener() {
		$this->getManager()->addListenerMeta(TestOneHandlerInterface::class, 'onOneEvent', 'oneEvent');
		$this->getManager()->addListenerMeta(TestTwoHandlerInterface::class, 'onTwoEvent', 'twoEvent');
		$this->getManager()->addListenerMeta(TestThreeHandlerInterface::class, 'onThreeEvent', 'threeEvent');

		$listener = new TestListener();
		$this->getManager()->addListener($listener);

		$this->assertTrue($this->getManager()->hasListeners('oneEvent'));
		$this->assertCount(1, $listeners = $this->getManager()->getListeners('oneEvent'));
		$this->assertEquals($listener, $listeners[0][0]);
		$this->assertEquals('onOneEvent', $listeners[0][1]);

		$this->assertTrue($this->getManager()->hasListeners('twoEvent'));
		$this->assertCount(1, $listeners = $this->getManager()->getListeners('twoEvent'));
		$this->assertEquals($listener, $listeners[0][0]);
		$this->assertEquals('onTwoEvent', $listeners[0][1]);

		$this->assertFalse($this->getManager()->hasListeners('threeEvent'));

	}

	public function testRemoveListener() {
		$this->getManager()->addListenerMeta(TestOneHandlerInterface::class, 'onOneEvent', 'oneEvent');
		$this->getManager()->addListenerMeta(TestTwoHandlerInterface::class, 'onTwoEvent', 'twoEvent');
		$this->getManager()->addListenerMeta(TestThreeHandlerInterface::class, 'onThreeEvent', 'threeEvent');

		$listener = new TestListener();
		$this->getManager()->addListener($listener);
		$this->getManager()->addListener(new class implements TestThreeHandlerInterface, TestTwoHandlerInterface {
			public function onThreeEvent() {
				// TODO: Implement onOneEvent() method.
			}

			public function onTwoEvent() {
				// TODO: Implement onTwoEvent() method.
			}
		});

		$this->getManager()->removeListener($listener);
		$this->assertFalse($this->getManager()->hasListeners('oneEvent'));
		$this->assertCount(0, $listeners = $this->getManager()->getListeners('oneEvent'));

		$this->assertTrue($this->getManager()->hasListeners('twoEvent'));
		$this->assertCount(1, $listeners = $this->getManager()->getListeners('twoEvent'));
		$this->assertNotEquals($listener, $listeners[0][0]);

		$this->assertTrue($this->getManager()->hasListeners('threeEvent'));
		$this->assertCount(1, $listeners = $this->getManager()->getListeners('threeEvent'));
	}

	public function testGetListeners() {
		$this->getManager()->addListenerMeta(TestOneHandlerInterface::class, 'onOneEvent', 'oneEvent');

		$this->assertCount(0, $this->getManager()->getListeners('oneEvent'));

		$listener = new TestListener();
		$this->getManager()->addListener($listener);

		$this->assertCount(1, $this->getManager()->getListeners('oneEvent'));
		$this->assertCount(0, $this->getManager()->getListeners('twoEvent'));
	}
}