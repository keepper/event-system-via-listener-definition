# Реализация ListenerManager в режиме игнорирования ошибок

В данной реализации, мы допускаем, что:

1. Повторное добавление мета данных, это из замещение

1. Игнорируем добавление слушателя, для которого не найдены мета данные

1. Игнорируем удаление не подписованного слушателя

## Реализация

Во избежания дублирования кода я не стал реализовывать `ListenerManager` с нуля, а лишь сделал, обертку над 
уже реализованным `StrictListenerManager`.

При этом объект `StrictListenerManager` обязателен для констуктора. Это связано с тем, что в классе обертке я не стал реализовывать
`ListenerProviderInterface` который будет необходим `Dispatcher`. 

Cмотрите реализацию [StrictListenerManager](../src/ListenerManager.php)

## Использование

```php
$strictListenerManager = new StrictListenerManager();

$listenerManager = new ListenerManager($strictListenerManager);

$dispatcher new MyDispatcher($strictListenerManager);

```

Подписка в данном случае, должна осуществляться через `$listenerManager`. Но реальным хранилищем подписчиков является 
`$strictListenerManager` 