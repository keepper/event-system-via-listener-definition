# Event system via listener definition

Система событий основаная на описании слушателя, а не объекта события.

## Статьи

1. [Альтернативный взгляд на систему событий](docs/001-WHY-ARTICLE.ru.md)
1. [Проектирование](docs/002-DESIGN.ru.md)
1. [Реализация StrictListenerManager](docs/003-StrictListenerManager.ru.md)
1. [Реализация ListenerManager](docs/004-ListenerManager.ru.md)
