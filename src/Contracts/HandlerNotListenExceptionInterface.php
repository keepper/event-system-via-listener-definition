<?php
namespace Keepper\Lib\EventSystem\Contracts;

/**
 * Исключение генерируемое при попытке удаления не подписанного слушателя
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
interface HandlerNotListenExceptionInterface {

}