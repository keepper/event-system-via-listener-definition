<?php
namespace Keepper\Lib\EventSystem\Contracts;

/**
 * Интерфейс позволяющий осуществлять подписку и отписку слушателей событий.
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
interface ListenerManagerInterface {

	/**
	 * Добавляет слушателя, как екземпляр класса реализующего конкретный интерфейс "Слушателя события"
	 * ListenerInterface - маркерный интерфейс
	 * @link https://gitlab.com/keepper/event-system-via-listener-definition/blob/master/docs/002-DESIGN.ru.md
	 *
	 * @param ListenerInterface $listener
	 * @throw HandlerMetaDataExceptionInterface
	 */
	public function addListener(ListenerInterface $listener): void;

	/**
	 * Удаляет слушателя.
	 * @throws HandlerNotListenExceptionInterface - В случае если указанный слушатель, не был подписан
	 */
	public function removeListener(ListenerInterface $listener): void;

}