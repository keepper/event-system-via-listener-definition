<?php
namespace Keepper\Lib\EventSystem\Contracts;

/**
 * Интерфейс ListenerInterface - маркерный интерфейс.
 * Данный интерфейс никогда не будет содержать описания методов.
 * Основное назначение облегчение поиска интерфейсов слушателей в проекте, по строке "extends" от данного интерфейса.
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
interface ListenerInterface {

}