<?php

namespace Keepper\Lib\EventSystem\Contracts;

/**
 * Исключение генерируемое при попытке добавления слушателя, мета данные которого не известны
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
interface HandlerMetaDataExceptionInterface {

}