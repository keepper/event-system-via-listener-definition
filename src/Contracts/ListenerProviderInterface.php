<?php

namespace Keepper\Lib\EventSystem\Contracts;

/**
 * Интерфейс, позволяющий получать слушателей по имени или иной инфорации о событии.
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
interface ListenerProviderInterface {

	/**
	 * Возвращает слушателей, для указанного события
	 * @return callable[]
	 */
	public function getListeners(string $event): array;

	/**
	 * Возвращает признак наличия слушателей указанного события
	 * @return bool
	 */
	public function hasListeners(string $event): bool;
}