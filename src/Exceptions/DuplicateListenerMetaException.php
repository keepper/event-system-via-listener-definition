<?php

namespace Keepper\Lib\EventSystem\Exceptions;

/**
 * Исключение генерируемое при попытке добавления дублирующихся мета данных о слушателях
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
class DuplicateListenerMetaException extends EventSystemException {

}