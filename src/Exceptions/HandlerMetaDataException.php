<?php

namespace Keepper\Lib\EventSystem\Exceptions;

use Keepper\Lib\EventSystem\Contracts\HandlerMetaDataExceptionInterface;

/**
 * @see HandlerMetaDataExceptionInterface
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
class HandlerMetaDataException extends EventSystemException implements HandlerMetaDataExceptionInterface {

}