<?php

namespace Keepper\Lib\EventSystem\Exceptions;

/**
 * Общий класс исключения для библиотеки
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
class EventSystemException extends \Exception {

}