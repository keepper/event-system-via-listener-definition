<?php

namespace Keepper\Lib\EventSystem\Exceptions;

use Keepper\Lib\EventSystem\Contracts\HandlerNotListenExceptionInterface;

/**
 * @see HandlerNotListenExceptionInterface
 *
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
class HandlerNotListenException extends EventSystemException implements HandlerNotListenExceptionInterface {

}