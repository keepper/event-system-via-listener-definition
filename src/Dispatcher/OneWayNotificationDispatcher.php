<?php

namespace Keepper\Lib\EventSystem\Dispatcher;

use Keepper\Lib\EventSystem\Contracts\ListenerProviderInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class OneWayNotificationDispatcher implements LoggerAwareInterface {
	use LoggerAwareTrait;

	protected $listenerProvider;

	public function __construct(ListenerProviderInterface $listenerProvider) {
		$this->listenerProvider = $listenerProvider;
		$this->setLogger(new NullLogger());
	}

	public function dispatch(string $eventName, ...$arguments) {
		if ( !$this->listenerProvider->hasListeners($eventName) ) {
			$this->logger->debug('Для события "'.$eventName.'" отсутствуют слушатели');
			return;
		}

		$listeners = $this->listenerProvider->getListeners($eventName);

		foreach ($listeners as $listener) {
			try {
				$listener(...$arguments);
			} catch (\Exception $e) {
				$this->logger->error('При вызове слушателя события "'.$eventName.'" произошло исключение. '.$e->getMessage());
				$this->logger->debug($e->getTraceAsString());
			}
		}
	}
}