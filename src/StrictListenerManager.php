<?php
namespace Keepper\Lib\EventSystem;

use Keepper\Lib\EventSystem\Contracts\ListenerInterface;
use Keepper\Lib\EventSystem\Contracts\ListenerManagerInterface;
use Keepper\Lib\EventSystem\Contracts\ListenerProviderInterface;
use Keepper\Lib\EventSystem\Exceptions\DuplicateListenerMetaException;
use Keepper\Lib\EventSystem\Exceptions\EventSystemException;
use Keepper\Lib\EventSystem\Exceptions\HandlerNotListenException;
use Keepper\Lib\EventSystem\Exceptions\HandlerMetaDataException;

/**
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
class StrictListenerManager implements ListenerManagerInterface, ListenerProviderInterface {

	private $listenerMeta = [];

	private $listeners = [];

	/**
	 * Добавляет мета данные для указанного интерфейса слушателя
	 * @param string $interfaceName
	 * @param string $methodName
	 * @param string $eventName
	 *
	 * @throws DuplicateListenerMetaException
	 * @throws EventSystemException
	 */
	public function addListenerMeta(string $interfaceName, string $methodName, string $eventName): void {
		if ($interfaceName == ListenerInterface::class) {
			throw new EventSystemException($interfaceName.' - является маркерным интерфейсом, и не подлежит использованию, как интерфейс реального слушателя');
		}

		if ( array_key_exists($interfaceName, $this->listenerMeta) ) {
			throw new DuplicateListenerMetaException('Мета данные для "'.$interfaceName.'" ранее уже были добавлены');
		}

		$this->listenerMeta[$interfaceName] = [$methodName, $eventName];
	}

	public function removeListenerMeta(string $interfaceName) {
		if ( isset($this->listenerMeta[$interfaceName]) ) {
			unset($this->listenerMeta[$interfaceName]);
		}
	}

	/**
	 * @inheritdoc
	 */
	public function addListener(ListenerInterface $listener): void {
		$hasMeta = false;

		foreach ($this->listenerMeta as $interfaceName => list($methodName, $eventName) ) {

			if ( !($listener instanceof $interfaceName) ) {
				continue;
			}

			$hasMeta = true;

			if ( !method_exists($listener, $methodName) ) {
				throw new HandlerMetaDataException('Не верные мета данные по "'.$interfaceName.'", не корректное название метода');
			}

			$this->addCallableListener($eventName, [$listener, $methodName]);
		}

		if ( !$hasMeta ) {
			throw new HandlerMetaDataException('Для указанного слушателя "'.get_class($listener).'" отсутствуют мета данные');
		}
	}

	/**
	 * Добавляет слушатель как callable сущность.
	 * @param string $eventName
	 * @param callable $listener
	 */
	public function addCallableListener(string $eventName, callable $listener): void {
		if ( !array_key_exists($eventName, $this->listeners) ) {
			$this->listeners[$eventName] = [];
		}

		$this->listeners[$eventName][] = $listener;
	}

	/**
	 * @inheritdoc
	 */
	public function removeListener(ListenerInterface $listener): void {
		$hasMeta = false;

		foreach ($this->listenerMeta as $interfaceName => list($methodName, $eventName) ) {

			if ( !($listener instanceof $interfaceName) ) {
				continue;
			}

			if ( !array_key_exists($eventName, $this->listeners) ) {
				continue;
			}

			$hasMeta = true;

			$listeners = [];
			foreach ($this->listeners[$eventName] as $callable) {
				if (is_array($callable) && $callable[0] == $listener) {
					continue;
				}
				$listeners[] = $callable;
			}
			$this->listeners[$eventName] = $listeners;
		}

		if ( !$hasMeta ) {
			throw new HandlerNotListenException('Указанный слушатель "'.get_class($listener).'" ранее не был подписан');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function getListeners(string $event): array {
		if ( !$this->hasListeners($event) ) {
			return [];
		}

		return $this->listeners[$event];
	}

	/**
	 * @inheritdoc
	 */
	public function hasListeners(string $event): bool {
		return array_key_exists($event, $this->listeners) && count($this->listeners[$event]) > 0;
	}
}