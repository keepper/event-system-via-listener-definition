<?php

namespace Keepper\Lib\EventSystem;

use Keepper\Lib\EventSystem\Contracts\ListenerInterface;
use Keepper\Lib\EventSystem\Contracts\ListenerManagerInterface;
use Keepper\Lib\EventSystem\Exceptions\HandlerMetaDataException;
use Keepper\Lib\EventSystem\Exceptions\HandlerNotListenException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * @package Keepper\Lib\EventSystem
 * @author Andrew Kosov (andrew.kosov@gmail.com)
 */
class ListenerManager implements ListenerManagerInterface, LoggerAwareInterface {

	use LoggerAwareTrait;

	/**
	 * @var StrictListenerManager
	 */
	private $strictListenerManager;

	public function __construct(
		StrictListenerManager $strictListenerManager
	) {
		$this->strictListenerManager = $strictListenerManager;
		$this->setLogger(new NullLogger());
	}

	/**
	 * Добавляет мета данные для указанного интерфейса слушателя
	 * @param string $interfaceName
	 * @param string $methodName
	 * @param string $eventName
	 */
	public function addListenerMeta(string $interfaceName, string $methodName, string $eventName): void {
		if ($interfaceName == ListenerInterface::class) {
			$this->logger->error($interfaceName.' - является маркерным интерфейсом, и не подлежит использованию, как интерфейс реального слушателя');
			return;
		}

		$this->strictListenerManager->removeListenerMeta($interfaceName);
		$this->strictListenerManager->addListenerMeta($interfaceName, $methodName, $eventName);
	}

	/**
	 * @inheritdoc
	 */
	public function addListener(ListenerInterface $listener): void {
		try {
			$this->strictListenerManager->addListener($listener);
		} catch (HandlerMetaDataException $e) {
			$this->logger->warning(__METHOD__ . ' ' . $e->getMessage()."\n".$e->getTraceAsString());
		}
	}

	/**
	 * @inheritdoc
	 */
	public function removeListener(ListenerInterface $listener): void {
		try {
			$this->strictListenerManager->removeListener($listener);
		} catch (HandlerNotListenException $e) {
			$this->logger->warning(__METHOD__ . ' ' . $e->getMessage()."\n".$e->getTraceAsString());
		}
	}
}